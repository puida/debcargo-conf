rust-lsd (1.0.0-2) unstable; urgency=medium

  * Team upload.
  * Package lsd 1.0.0 from crates.io using debcargo 2.6.1
  * Bump terminal-size dependency to 0.3

 -- Peter Michael Green <plugwash@debian.org>  Thu, 25 Jan 2024 09:03:11 +0000

rust-lsd (1.0.0-1) unstable; urgency=medium

  * Team upload.
  * Package lsd 1.0.0 from crates.io using debcargo 2.6.1
  * Adjust patches for new upstream.
  * Bump git2 dependency.
  * Bump lscolors dependency to 0.16.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 07 Jan 2024 11:20:44 +0000

rust-lsd (0.23.1-8) unstable; urgency=medium

  * Team upload.
  * Package lsd 0.23.1 from crates.io using debcargo 2.6.0
  * Reduce unnessacery dependency patching.
  * Bump crossterm to 0.27

 -- Peter Michael Green <plugwash@debian.org>  Thu, 23 Nov 2023 04:50:55 +0000

rust-lsd (0.23.1-7) unstable; urgency=medium

  * Team upload.
  * Package lsd 0.23.1 from crates.io using debcargo 2.6.0
  * Adjust dependency on serde-yaml to allow any 0.x version greater than 0.8,
    this does carry some risk, but I think it's better than the alternatives.
  * Allow versions 3.x, 4.x and 5.x of dirs crate.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 29 Jul 2023 13:24:12 +0000

rust-lsd (0.23.1-6) unstable; urgency=medium

  * Team upload.
  * Package lsd 0.23.1 from crates.io using debcargo 2.6.0
  * Cleanup patches a bit.
  * Bump chrono-humanize dependency to 2.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 20 Jul 2023 15:42:44 +0000

rust-lsd (0.23.1-5) unstable; urgency=medium

  * Team upload.
  * Package lsd 0.23.1 from crates.io using debcargo 2.6.0
  * Unbreak the build with the xattr update
  * Remove windows deps

  [ Alexander Kjäll ]
  * Package lsd 0.23.1 from crates.io using debcargo 2.6.0
  * Adds recommendation to use fonts-font-awesome (closes: 1037255)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 09 Jul 2023 12:38:12 +0200

rust-lsd (0.23.1-4) unstable; urgency=medium

  * Team upload.
  * Package lsd 0.23.1 from crates.io using debcargo 2.6.0
  * Bump lscolors dependency to 0.14

 -- Peter Michael Green <plugwash@debian.org>  Sat, 08 Jul 2023 13:29:16 +0000

rust-lsd (0.23.1-3) unstable; urgency=medium

  * Package lsd 0.23.1 from crates.io using debcargo 2.6.0
  * Add shell completion files for bash, fish and zsh (Closes: #1028645)

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Fri, 27 Jan 2023 09:57:30 +0100

rust-lsd (0.23.1-2) unstable; urgency=medium

  * Team upload.
  * Package lsd 0.23.1 from crates.io using debcargo 2.6.0
  * Bump lscolors dependency to 0.13 (Closes: #1028628)

 -- Peter Michael Green <plugwash@debian.org>  Sat, 14 Jan 2023 01:10:20 +0000

rust-lsd (0.23.1-1) unstable; urgency=medium

  * Package lsd 0.23.1 from crates.io using debcargo 2.6.0

  [ James Hendry ]
  * Team upload.
  * Package lsd 0.22.0 from crates.io using debcargo 2.5.0

  [ heinrich5991 ]
  * Package lsd 0.21.0 from crates.io using debcargo 2.5.0

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Fri, 06 Jan 2023 17:14:35 +0100
